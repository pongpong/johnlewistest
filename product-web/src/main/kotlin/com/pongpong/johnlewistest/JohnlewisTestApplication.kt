package com.pongpong.johnlewistest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JohnlewisTestApplication

fun main(args: Array<String>) {
	runApplication<JohnlewisTestApplication>(*args)
}
