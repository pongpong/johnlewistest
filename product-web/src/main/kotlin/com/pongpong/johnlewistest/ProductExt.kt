package com.pongpong.johnlewistest

import com.pongpong.johnlewistest.products.types.Product
import com.pongpong.johnlewistest.restful.types.ColorSwatch
import com.pongpong.johnlewistest.restful.types.DiscountProducts

fun Product.transform(labelType: String): DiscountProducts {
    val p = DiscountProducts()
    p.productId = this.productId
    p.title = this.title
    p.colorSwatches = this.colorSwatches.map {
        val cs = ColorSwatch()
        cs.color = it.color
        cs.rgbColor = it.basicColorHex()
        cs.skuid = it.skuId
        cs
    }
    p.nowPrice = this.price.now.format(this.price.symbol())
    p.priceLabel = this.price.toPriceLabel(labelType)
    return p
}
