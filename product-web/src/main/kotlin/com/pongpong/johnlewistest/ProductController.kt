package com.pongpong.johnlewistest

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.jackson.responseObject
import com.github.kittinunf.result.Result
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import com.pongpong.johnlewistest.products.types.Products
import com.pongpong.johnlewistest.restful.types.DiscountProducts
import org.springframework.web.bind.annotation.RequestParam

@RestController
class ProductController {
    val BASE_URL = "https://api.johnlewis.com/"
    val KEY = "AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
    val OPERATION = "search/api/rest/v2/catalog/products/search/keyword"
    var QUERY = "dresses"

    @GetMapping("/products/discount")
    fun products(
        @RequestParam(value = "labelType", defaultValue = SHOW_WAS_NOW)
        labelType: String
    ): List<DiscountProducts> {
        val url = BASE_URL + OPERATION;
        val (_, _, result) = url.httpGet(listOf("q" to QUERY, "key" to KEY))
            .responseObject<Products>()
        when (result) {
            is Result.Success -> {
                return result.get().products.filter {
                    it.price.isReduced()
                }.sortedByDescending {
                    it.price.priceReduction()
                }.map {
                    it.transform(labelType)
                }
            }
            is Result.Failure -> {
                throw result.getException()
            }
        }
    }
}

