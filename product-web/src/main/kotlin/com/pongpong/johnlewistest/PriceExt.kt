package com.pongpong.johnlewistest

import com.pongpong.johnlewistest.products.types.Now
import com.pongpong.johnlewistest.products.types.Price
import java.lang.UnsupportedOperationException
import java.util.*

const val SHOW_WAS_NOW = "ShowWasNow"
const val SHOW_WAS_THEN_NOW = "ShowWasThenNow"
const val SHOW_PERC_DSCOUNT = "ShowPercDscount"

private fun Double.formatPrice(digits: Int, lessThan: Double): String {
    val d = if (this < lessThan) digits else 0
    return "%.${d}f".format(this)
}


private fun Double.formatPercentage():String {
    return "%.0f".format(this)
}

private fun String.formatPrice(): String {
    return if(this.isEmpty()) "0.00"
    else this.toDouble().formatPrice(2, 10.0)
}

private fun String.was(symbol: String): String {
    return "Was ${symbol}${this.formatPrice()}, "
}

private fun String.then(symbol: String): String {
    return if (this.isEmpty()) "" else "then ${symbol}${this.formatPrice()}, "
}

private fun String.symbol(): String {
    return Currency.getInstance(this).getSymbol(
        Locale.getDefault(Locale.Category.DISPLAY)
    )
}

fun Now.formatPriceLabel(symbol: String): String {
    return "now ${format(symbol)}"
}

fun Now.format(symbol:String): String {
    return symbol + if(this.fixed.isNullOrBlank())
        "${this.from.formatPrice()}-${this.to.formatPrice()}"
    else
        this.fixed.formatPrice()
}


fun Price.symbol(): String = this.currency.symbol()

fun Price.priceReduction(): Double {
    val was = if(this.was.isNullOrBlank()) 0.0 else this.was.toDouble()
    val now = if(this.now.fixed.isNullOrEmpty()) this.now.from.toDouble()
    else this.now.fixed.toDouble()
    return was - now
}

fun Price.isReduced():Boolean {
    return priceReduction() > 0
}


fun Price.toPriceLabel(labelType: String): String {
    val res: String
    val symbol = symbol()
    when (labelType) {
        SHOW_WAS_NOW -> {
            res = was.was(symbol) + now.formatPriceLabel(symbol)
        }
        SHOW_WAS_THEN_NOW -> {
            val then = if (this.then2.isNullOrEmpty()) then1 else then2
            res = was.was(symbol) + then.then(symbol) + now.formatPriceLabel(symbol)
        }
        SHOW_PERC_DSCOUNT -> {
            res = if (was.isNullOrEmpty()) {
                now.formatPriceLabel(symbol)
            } else {
                val wasVal = was.toDouble()
                val nowVal = if (now.fixed.isNullOrEmpty())
                    now.from.toDouble() else now.fixed.toDouble()
                val pc = if (nowVal < wasVal) (1.0 - nowVal / wasVal) * 100.0 else 0.0
                "${pc.formatPercentage()}% off - " + now.formatPriceLabel(symbol)
            }
        }
        else -> {
            throw UnsupportedOperationException("invalid labelType")
        }
    }
    return res
}