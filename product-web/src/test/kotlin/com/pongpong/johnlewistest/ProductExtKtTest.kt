package com.pongpong.johnlewistest

import com.pongpong.johnlewistest.products.types.ColorSwatch
import com.pongpong.johnlewistest.products.types.Now
import com.pongpong.johnlewistest.products.types.Price
import com.pongpong.johnlewistest.products.types.Product
import org.junit.jupiter.api.Test
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.hamcrest.CoreMatchers.`is` as Is

import org.junit.jupiter.api.Assertions.*

internal class ProductExtKtTest {

    @Test
    fun transform() {
        val p = Product()
        p.productId = "4761678"
        p.title = "Mint Velvet Bonnie High Neck Mini Dress, Multi"
        p.price = Price("99.00", "59.00", "45.00",
            Now("35.00"), "", "GBP"
        )
        p.colorSwatches = listOf(ColorSwatch(
            "Burgundy",
        "Green",
        "//johnlewis.scene7.com/is/image/JohnLewis/004747398?cropN=0.42105263157894735,0.42105263157894735,0.16842105263157894,0.16842105263157894&",
        "//johnlewis.scene7.com/is/image/JohnLewis/004747398?",
        true,
        "238368223"
        ))

        val res = p.transform(SHOW_WAS_NOW)
        assertThat(res, allOf(
            hasProperty("productId", Is("4761678")),
            hasProperty("title", Is("Mint Velvet Bonnie High Neck Mini Dress, Multi")),
            hasProperty("colorSwatches",
                containsInAnyOrder<com.pongpong.johnlewistest.restful.types.ColorSwatch>(
                    allOf(
                        hasProperty("color", Is("Burgundy")),
                        hasProperty("rgbColor", Is("008000")),
                        hasProperty("skuid", Is("238368223")),
                    )
                )),
            hasProperty("nowPrice", Is("£35")),
            hasProperty("priceLabel", Is("Was £99, now £35")),
        ))
    }
}