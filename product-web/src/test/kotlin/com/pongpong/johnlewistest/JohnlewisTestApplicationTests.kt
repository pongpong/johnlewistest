package com.pongpong.johnlewistest

import com.github.kittinunf.fuel.core.Client
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.any
import org.springframework.boot.test.context.SpringBootTest
import java.io.ByteArrayInputStream
import java.net.URL

@SpringBootTest
class JohnlewisTestApplicationTests {
	@BeforeEach
	internal fun setUp() {
		FuelManager.instance.client = object: Client {
			override fun executeRequest(request: Request): Response {
				val reqUrl =URL("https://api.johnlewis.com/")
				return Response(reqUrl, 200, sampleResponse())
			}
		}
	}

	fun sampleResponse() = JohnlewisTestApplicationTests::class
		.java.getResource("/test-response.html").readText()

	@Test
	fun contextLoads() {
	}

}
