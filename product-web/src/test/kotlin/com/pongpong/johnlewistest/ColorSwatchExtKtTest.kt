package com.pongpong.johnlewistest

import com.pongpong.johnlewistest.products.types.ColorSwatch
import org.junit.jupiter.api.Test

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is` as Is
import org.junit.jupiter.api.Assertions.*

internal class ColorSwatchExtKtTest {
    fun newColorSwatch(basicColor:String?):ColorSwatch {
       return ColorSwatch("", basicColor, "12345", "", true, "")
    }

    @Test
    fun basicColorHex() {
        assertThat(newColorSwatch("RED").basicColorHex(), Is("FF0000"))
        assertThat(newColorSwatch("red").basicColorHex(), Is("FF0000"))
        assertThat(newColorSwatch("GREEN").basicColorHex(), Is("008000"))
        assertThat(newColorSwatch("green").basicColorHex(), Is("008000"))
        assertThat(newColorSwatch("BLUE").basicColorHex(), Is("0000FF"))
        assertThat(newColorSwatch("blue").basicColorHex(), Is("0000FF"))
        assertThat(newColorSwatch("invalid").basicColorHex(), Is(""))
        assertThat(newColorSwatch("    ").basicColorHex(), Is(""))
        assertThat(newColorSwatch("").basicColorHex(), Is(""))
        assertThat(newColorSwatch(null).basicColorHex(), Is(""))
        assertThat(newColorSwatch("Black").basicColorHex(), Is("000000"))
        assertThat(newColorSwatch("Grey").basicColorHex(), Is("808080"))
        assertThat(newColorSwatch("Multi").basicColorHex(), Is(""))
        assertThat(newColorSwatch("Natural").basicColorHex(), Is(""))
        assertThat(newColorSwatch("Pink").basicColorHex(), Is("FFC0CB"))
    }
}