package com.pongpong.johnlewistest

import com.pongpong.johnlewistest.products.types.Now
import com.pongpong.johnlewistest.products.types.Price
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.CoreMatchers.`is` as Is
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PriceExtKtTest {

    @Test
    fun toPriceLabel_SHOW_WAS_NOW() {
        var res = Price("", "", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_NOW)
        assertThat(res, Is("Was £0.00, now £165"))
        res = Price("199", "", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_NOW)
        assertThat(res, Is("Was £199, now £165"))
        res = Price("199", "", "", Now("160", "165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_NOW)
        assertThat(res, Is("Was £199, now £160-165"))
        // below £10
        res = Price("9", "", "", Now("7", "8"), "", "GBP")
            .toPriceLabel(SHOW_WAS_NOW)
        assertThat(res, Is("Was £9.00, now £7.00-8.00"))
    }

    @Test
    fun toPriceLabel_SHOW_WAS_THEN_NOW() {
        // fallback to then1
        var res = Price("", "150", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_THEN_NOW)
        assertThat(res, Is("Was £0.00, then £150, now £165"))
        res = Price("", "150", "160", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_THEN_NOW)
        assertThat(res, Is("Was £0.00, then £160, now £165"))
        res = Price("140", "150", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_THEN_NOW)
        assertThat(res, Is("Was £140, then £150, now £165"))
        res = Price("140", "150", "160", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_WAS_THEN_NOW)
        assertThat(res, Is("Was £140, then £160, now £165"))
        // below £10
        res = Price("4.0", "3", "2", Now("1"), "", "GBP")
            .toPriceLabel(SHOW_WAS_THEN_NOW)
        assertThat(res, Is("Was £4.00, then £2.00, now £1.00"))
    }

    @Test
    fun toPriceLabel_SHOW_PERC_DSCOUNT() {
        var res = Price("", "", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_PERC_DSCOUNT)
        assertThat(res, Is("now £165"))
        res = Price("180", "", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_PERC_DSCOUNT)
        assertThat(res, Is("8% off - now £165"))
        res = Price("140", "", "", Now("165"), "", "GBP")
            .toPriceLabel(SHOW_PERC_DSCOUNT)
        assertThat(res, Is("0% off - now £165"))
        // below £10
        res = Price("140", "", "", Now("9"), "", "GBP")
            .toPriceLabel(SHOW_PERC_DSCOUNT)
        assertThat(res, Is("94% off - now £9.00"))
    }

    @Test
    fun isPriceReduction() {
        var res = Price("140", "", "", Now("9"), "", "GBP")
            .isReduced()
        assertThat(res, Is(true))
        res = Price("9", "", "", Now("9"), "", "GBP")
            .isReduced()
        assertThat(res, Is(false))
        res = Price("9", "", "", Now("10"), "", "GBP")
            .isReduced()
        assertThat(res, Is(false))
        res = Price("", "", "", Now("10"), "", "GBP")
            .isReduced()
        assertThat(res, Is(false))
    }
}