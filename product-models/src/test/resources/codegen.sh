#!/bin/sh

# b: generate builder
# c: generate constructor
# a: generate jackson2 annotation
# p: package name
# ft: format date time
# R: remove-old-output
# sl: serializable
jsonschema2pojo \
-b \
-c \
-a JACKSON2 \
-ft \
-p com.pongpong.johnlewistest \
-R \
-sl \
-T JSON \
-tl JAVA \
-s sample.json \
-t ../../../generated/src/main/java/





