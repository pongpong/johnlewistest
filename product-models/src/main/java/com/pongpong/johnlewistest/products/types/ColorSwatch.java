
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "color",
    "basicColor",
    "colorSwatchUrl",
    "imageUrl",
    "isAvailable",
    "skuId"
})
public class ColorSwatch {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    private String color;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("basicColor")
    private String basicColor;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    private String colorSwatchUrl;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("imageUrl")
    private String imageUrl;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isAvailable")
    private Boolean isAvailable;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("skuId")
    private String skuId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ColorSwatch() {
    }

    /**
     * 
     * @param basicColor
     * @param isAvailable
     * @param colorSwatchUrl
     * @param color
     * @param imageUrl
     * @param skuId
     */
    public ColorSwatch(String color, String basicColor, String colorSwatchUrl, String imageUrl, Boolean isAvailable, String skuId) {
        super();
        this.color = color;
        this.basicColor = basicColor;
        this.colorSwatchUrl = colorSwatchUrl;
        this.imageUrl = imageUrl;
        this.isAvailable = isAvailable;
        this.skuId = skuId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    public ColorSwatch withColor(String color) {
        this.color = color;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("basicColor")
    public String getBasicColor() {
        return basicColor;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("basicColor")
    public void setBasicColor(String basicColor) {
        this.basicColor = basicColor;
    }

    public ColorSwatch withBasicColor(String basicColor) {
        this.basicColor = basicColor;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    public String getColorSwatchUrl() {
        return colorSwatchUrl;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    public void setColorSwatchUrl(String colorSwatchUrl) {
        this.colorSwatchUrl = colorSwatchUrl;
    }

    public ColorSwatch withColorSwatchUrl(String colorSwatchUrl) {
        this.colorSwatchUrl = colorSwatchUrl;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("imageUrl")
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("imageUrl")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ColorSwatch withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isAvailable")
    public Boolean getIsAvailable() {
        return isAvailable;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isAvailable")
    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public ColorSwatch withIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("skuId")
    public String getSkuId() {
        return skuId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("skuId")
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public ColorSwatch withSkuId(String skuId) {
        this.skuId = skuId;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ColorSwatch withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ColorSwatch.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("color");
        sb.append('=');
        sb.append(((this.color == null)?"<null>":this.color));
        sb.append(',');
        sb.append("basicColor");
        sb.append('=');
        sb.append(((this.basicColor == null)?"<null>":this.basicColor));
        sb.append(',');
        sb.append("colorSwatchUrl");
        sb.append('=');
        sb.append(((this.colorSwatchUrl == null)?"<null>":this.colorSwatchUrl));
        sb.append(',');
        sb.append("imageUrl");
        sb.append('=');
        sb.append(((this.imageUrl == null)?"<null>":this.imageUrl));
        sb.append(',');
        sb.append("isAvailable");
        sb.append('=');
        sb.append(((this.isAvailable == null)?"<null>":this.isAvailable));
        sb.append(',');
        sb.append("skuId");
        sb.append('=');
        sb.append(((this.skuId == null)?"<null>":this.skuId));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.basicColor == null)? 0 :this.basicColor.hashCode()));
        result = ((result* 31)+((this.isAvailable == null)? 0 :this.isAvailable.hashCode()));
        result = ((result* 31)+((this.colorSwatchUrl == null)? 0 :this.colorSwatchUrl.hashCode()));
        result = ((result* 31)+((this.color == null)? 0 :this.color.hashCode()));
        result = ((result* 31)+((this.imageUrl == null)? 0 :this.imageUrl.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.skuId == null)? 0 :this.skuId.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ColorSwatch) == false) {
            return false;
        }
        ColorSwatch rhs = ((ColorSwatch) other);
        return ((((((((this.basicColor == rhs.basicColor)||((this.basicColor!= null)&&this.basicColor.equals(rhs.basicColor)))&&((this.isAvailable == rhs.isAvailable)||((this.isAvailable!= null)&&this.isAvailable.equals(rhs.isAvailable))))&&((this.colorSwatchUrl == rhs.colorSwatchUrl)||((this.colorSwatchUrl!= null)&&this.colorSwatchUrl.equals(rhs.colorSwatchUrl))))&&((this.color == rhs.color)||((this.color!= null)&&this.color.equals(rhs.color))))&&((this.imageUrl == rhs.imageUrl)||((this.imageUrl!= null)&&this.imageUrl.equals(rhs.imageUrl))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.skuId == rhs.skuId)||((this.skuId!= null)&&this.skuId.equals(rhs.skuId))));
    }

}
