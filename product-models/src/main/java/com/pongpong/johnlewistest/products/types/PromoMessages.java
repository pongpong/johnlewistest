
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reducedToClear",
    "priceMatched",
    "offer",
    "customPromotionalMessage",
    "bundleHeadline",
    "customSpecialOffer"
})
public class PromoMessages {

    @JsonProperty("reducedToClear")
    private Boolean reducedToClear;
    @JsonProperty("priceMatched")
    private String priceMatched;
    @JsonProperty("offer")
    private String offer;
    @JsonProperty("customPromotionalMessage")
    private String customPromotionalMessage;
    @JsonProperty("bundleHeadline")
    private String bundleHeadline;
    @JsonProperty("customSpecialOffer")
    private CustomSpecialOffer customSpecialOffer;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public PromoMessages() {
    }

    /**
     *
     * @param offer
     * @param customSpecialOffer
     * @param bundleHeadline
     * @param reducedToClear
     * @param customPromotionalMessage
     * @param priceMatched
     */
    public PromoMessages(Boolean reducedToClear, String priceMatched, String offer, String customPromotionalMessage, String bundleHeadline, CustomSpecialOffer customSpecialOffer) {
        super();
        this.reducedToClear = reducedToClear;
        this.priceMatched = priceMatched;
        this.offer = offer;
        this.customPromotionalMessage = customPromotionalMessage;
        this.bundleHeadline = bundleHeadline;
        this.customSpecialOffer = customSpecialOffer;
    }

    @JsonProperty("reducedToClear")
    public Boolean getReducedToClear() {
        return reducedToClear;
    }

    @JsonProperty("reducedToClear")
    public void setReducedToClear(Boolean reducedToClear) {
        this.reducedToClear = reducedToClear;
    }

    public PromoMessages withReducedToClear(Boolean reducedToClear) {
        this.reducedToClear = reducedToClear;
        return this;
    }

    @JsonProperty("priceMatched")
    public String getPriceMatched() {
        return priceMatched;
    }

    @JsonProperty("priceMatched")
    public void setPriceMatched(String priceMatched) {
        this.priceMatched = priceMatched;
    }

    public PromoMessages withPriceMatched(String priceMatched) {
        this.priceMatched = priceMatched;
        return this;
    }

    @JsonProperty("offer")
    public String getOffer() {
        return offer;
    }

    @JsonProperty("offer")
    public void setOffer(String offer) {
        this.offer = offer;
    }

    public PromoMessages withOffer(String offer) {
        this.offer = offer;
        return this;
    }

    @JsonProperty("customPromotionalMessage")
    public String getCustomPromotionalMessage() {
        return customPromotionalMessage;
    }

    @JsonProperty("customPromotionalMessage")
    public void setCustomPromotionalMessage(String customPromotionalMessage) {
        this.customPromotionalMessage = customPromotionalMessage;
    }

    public PromoMessages withCustomPromotionalMessage(String customPromotionalMessage) {
        this.customPromotionalMessage = customPromotionalMessage;
        return this;
    }

    @JsonProperty("bundleHeadline")
    public String getBundleHeadline() {
        return bundleHeadline;
    }

    @JsonProperty("bundleHeadline")
    public void setBundleHeadline(String bundleHeadline) {
        this.bundleHeadline = bundleHeadline;
    }

    public PromoMessages withBundleHeadline(String bundleHeadline) {
        this.bundleHeadline = bundleHeadline;
        return this;
    }

    @JsonProperty("customSpecialOffer")
    public CustomSpecialOffer getCustomSpecialOffer() {
        return customSpecialOffer;
    }

    @JsonProperty("customSpecialOffer")
    public void setCustomSpecialOffer(CustomSpecialOffer customSpecialOffer) {
        this.customSpecialOffer = customSpecialOffer;
    }

    public PromoMessages withCustomSpecialOffer(CustomSpecialOffer customSpecialOffer) {
        this.customSpecialOffer = customSpecialOffer;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PromoMessages withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(PromoMessages.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("reducedToClear");
        sb.append('=');
        sb.append(((this.reducedToClear == null)?"<null>":this.reducedToClear));
        sb.append(',');
        sb.append("priceMatched");
        sb.append('=');
        sb.append(((this.priceMatched == null)?"<null>":this.priceMatched));
        sb.append(',');
        sb.append("offer");
        sb.append('=');
        sb.append(((this.offer == null)?"<null>":this.offer));
        sb.append(',');
        sb.append("customPromotionalMessage");
        sb.append('=');
        sb.append(((this.customPromotionalMessage == null)?"<null>":this.customPromotionalMessage));
        sb.append(',');
        sb.append("bundleHeadline");
        sb.append('=');
        sb.append(((this.bundleHeadline == null)?"<null>":this.bundleHeadline));
        sb.append(',');
        sb.append("customSpecialOffer");
        sb.append('=');
        sb.append(((this.customSpecialOffer == null)?"<null>":this.customSpecialOffer));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.offer == null)? 0 :this.offer.hashCode()));
        result = ((result* 31)+((this.customSpecialOffer == null)? 0 :this.customSpecialOffer.hashCode()));
        result = ((result* 31)+((this.bundleHeadline == null)? 0 :this.bundleHeadline.hashCode()));
        result = ((result* 31)+((this.reducedToClear == null)? 0 :this.reducedToClear.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.customPromotionalMessage == null)? 0 :this.customPromotionalMessage.hashCode()));
        result = ((result* 31)+((this.priceMatched == null)? 0 :this.priceMatched.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PromoMessages) == false) {
            return false;
        }
        PromoMessages rhs = ((PromoMessages) other);
        return ((((((((this.offer == rhs.offer)||((this.offer!= null)&&this.offer.equals(rhs.offer)))&&((this.customSpecialOffer == rhs.customSpecialOffer)||((this.customSpecialOffer!= null)&&this.customSpecialOffer.equals(rhs.customSpecialOffer))))&&((this.bundleHeadline == rhs.bundleHeadline)||((this.bundleHeadline!= null)&&this.bundleHeadline.equals(rhs.bundleHeadline))))&&((this.reducedToClear == rhs.reducedToClear)||((this.reducedToClear!= null)&&this.reducedToClear.equals(rhs.reducedToClear))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.customPromotionalMessage == rhs.customPromotionalMessage)||((this.customPromotionalMessage!= null)&&this.customPromotionalMessage.equals(rhs.customPromotionalMessage))))&&((this.priceMatched == rhs.priceMatched)||((this.priceMatched!= null)&&this.priceMatched.equals(rhs.priceMatched))));
    }

}
