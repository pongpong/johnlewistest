package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import java.io.IOException;

public class NowJsonDeserializer extends JsonDeserializer<Now> {
    @Override
    public Now deserialize(JsonParser jp,
                           DeserializationContext deserializationContext)
            throws IOException, JsonProcessingException {
        // Get reference to ObjectCodec
        ObjectCodec codec = jp.getCodec();

        // Parse "object" node into Jackson's tree model
        JsonNode node = codec.readTree(jp);

        if (node.getNodeType().equals(JsonNodeType.STRING)) {
            return new Now(node.asText());
        } else {
            return new Now(node.get("from").asText(), node.get("to").asText());
        }
    }
}
