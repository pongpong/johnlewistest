
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "showInStockOnly",
    "products",
    "facets",
    "results",
    "pagesAvailable",
    "crumbs",
    "pageInformation",
    "baseFacetId"
})
public class Products {

    @JsonProperty("showInStockOnly")
    private Boolean showInStockOnly;
    @JsonProperty("products")
    private List<Product> products = new ArrayList<Product>();
    @JsonProperty("facets")
    private List<Facet> facets = new ArrayList<Facet>();
    @JsonProperty("results")
    private Double results;
    @JsonProperty("pagesAvailable")
    private Double pagesAvailable;
    @JsonProperty("crumbs")
    private List<Crumb> crumbs = new ArrayList<Crumb>();
    @JsonProperty("pageInformation")
    private PageInformation pageInformation;
    @JsonProperty("baseFacetId")
    private String baseFacetId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Products() {
    }

    /**
     * 
     * @param baseFacetId
     * @param crumbs
     * @param pagesAvailable
     * @param pageInformation
     * @param showInStockOnly
     * @param results
     * @param products
     * @param facets
     */
    public Products(Boolean showInStockOnly, List<Product> products, List<Facet> facets, Double results, Double pagesAvailable, List<Crumb> crumbs, PageInformation pageInformation, String baseFacetId) {
        super();
        this.showInStockOnly = showInStockOnly;
        this.products = products;
        this.facets = facets;
        this.results = results;
        this.pagesAvailable = pagesAvailable;
        this.crumbs = crumbs;
        this.pageInformation = pageInformation;
        this.baseFacetId = baseFacetId;
    }

    @JsonProperty("showInStockOnly")
    public Boolean getShowInStockOnly() {
        return showInStockOnly;
    }

    @JsonProperty("showInStockOnly")
    public void setShowInStockOnly(Boolean showInStockOnly) {
        this.showInStockOnly = showInStockOnly;
    }

    public Products withShowInStockOnly(Boolean showInStockOnly) {
        this.showInStockOnly = showInStockOnly;
        return this;
    }

    @JsonProperty("products")
    public List<Product> getProducts() {
        return products;
    }

    @JsonProperty("products")
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Products withProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    @JsonProperty("facets")
    public List<Facet> getFacets() {
        return facets;
    }

    @JsonProperty("facets")
    public void setFacets(List<Facet> facets) {
        this.facets = facets;
    }

    public Products withFacets(List<Facet> facets) {
        this.facets = facets;
        return this;
    }

    @JsonProperty("results")
    public Double getResults() {
        return results;
    }

    @JsonProperty("results")
    public void setResults(Double results) {
        this.results = results;
    }

    public Products withResults(Double results) {
        this.results = results;
        return this;
    }

    @JsonProperty("pagesAvailable")
    public Double getPagesAvailable() {
        return pagesAvailable;
    }

    @JsonProperty("pagesAvailable")
    public void setPagesAvailable(Double pagesAvailable) {
        this.pagesAvailable = pagesAvailable;
    }

    public Products withPagesAvailable(Double pagesAvailable) {
        this.pagesAvailable = pagesAvailable;
        return this;
    }

    @JsonProperty("crumbs")
    public List<Crumb> getCrumbs() {
        return crumbs;
    }

    @JsonProperty("crumbs")
    public void setCrumbs(List<Crumb> crumbs) {
        this.crumbs = crumbs;
    }

    public Products withCrumbs(List<Crumb> crumbs) {
        this.crumbs = crumbs;
        return this;
    }

    @JsonProperty("pageInformation")
    public PageInformation getPageInformation() {
        return pageInformation;
    }

    @JsonProperty("pageInformation")
    public void setPageInformation(PageInformation pageInformation) {
        this.pageInformation = pageInformation;
    }

    public Products withPageInformation(PageInformation pageInformation) {
        this.pageInformation = pageInformation;
        return this;
    }

    @JsonProperty("baseFacetId")
    public String getBaseFacetId() {
        return baseFacetId;
    }

    @JsonProperty("baseFacetId")
    public void setBaseFacetId(String baseFacetId) {
        this.baseFacetId = baseFacetId;
    }

    public Products withBaseFacetId(String baseFacetId) {
        this.baseFacetId = baseFacetId;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Products withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Products.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("showInStockOnly");
        sb.append('=');
        sb.append(((this.showInStockOnly == null)?"<null>":this.showInStockOnly));
        sb.append(',');
        sb.append("products");
        sb.append('=');
        sb.append(((this.products == null)?"<null>":this.products));
        sb.append(',');
        sb.append("facets");
        sb.append('=');
        sb.append(((this.facets == null)?"<null>":this.facets));
        sb.append(',');
        sb.append("results");
        sb.append('=');
        sb.append(((this.results == null)?"<null>":this.results));
        sb.append(',');
        sb.append("pagesAvailable");
        sb.append('=');
        sb.append(((this.pagesAvailable == null)?"<null>":this.pagesAvailable));
        sb.append(',');
        sb.append("crumbs");
        sb.append('=');
        sb.append(((this.crumbs == null)?"<null>":this.crumbs));
        sb.append(',');
        sb.append("pageInformation");
        sb.append('=');
        sb.append(((this.pageInformation == null)?"<null>":this.pageInformation));
        sb.append(',');
        sb.append("baseFacetId");
        sb.append('=');
        sb.append(((this.baseFacetId == null)?"<null>":this.baseFacetId));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.baseFacetId == null)? 0 :this.baseFacetId.hashCode()));
        result = ((result* 31)+((this.crumbs == null)? 0 :this.crumbs.hashCode()));
        result = ((result* 31)+((this.pagesAvailable == null)? 0 :this.pagesAvailable.hashCode()));
        result = ((result* 31)+((this.pageInformation == null)? 0 :this.pageInformation.hashCode()));
        result = ((result* 31)+((this.showInStockOnly == null)? 0 :this.showInStockOnly.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.results == null)? 0 :this.results.hashCode()));
        result = ((result* 31)+((this.products == null)? 0 :this.products.hashCode()));
        result = ((result* 31)+((this.facets == null)? 0 :this.facets.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Products) == false) {
            return false;
        }
        Products rhs = ((Products) other);
        return ((((((((((this.baseFacetId == rhs.baseFacetId)||((this.baseFacetId!= null)&&this.baseFacetId.equals(rhs.baseFacetId)))&&((this.crumbs == rhs.crumbs)||((this.crumbs!= null)&&this.crumbs.equals(rhs.crumbs))))&&((this.pagesAvailable == rhs.pagesAvailable)||((this.pagesAvailable!= null)&&this.pagesAvailable.equals(rhs.pagesAvailable))))&&((this.pageInformation == rhs.pageInformation)||((this.pageInformation!= null)&&this.pageInformation.equals(rhs.pageInformation))))&&((this.showInStockOnly == rhs.showInStockOnly)||((this.showInStockOnly!= null)&&this.showInStockOnly.equals(rhs.showInStockOnly))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.results == rhs.results)||((this.results!= null)&&this.results.equals(rhs.results))))&&((this.products == rhs.products)||((this.products!= null)&&this.products.equals(rhs.products))))&&((this.facets == rhs.facets)||((this.facets!= null)&&this.facets.equals(rhs.facets))));
    }

}
