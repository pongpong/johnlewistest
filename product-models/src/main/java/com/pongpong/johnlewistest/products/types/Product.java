
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "productId",
    "type",
    "title",
    "htmlTitle",
    "code",
    "averageRating",
    "reviews",
    "price",
    "image",
    "displaySpecialOffer",
    "promoMessages",
    "nonPromoMessage",
    "defaultSkuId",
    "colorSwatches",
    "colorSwatchSelected",
    "colorWheelMessage",
    "outOfStock",
    "emailMeWhenAvailable",
    "availabilityMessage",
    "compare",
    "fabric",
    "swatchAvailable",
    "categoryQuickViewEnabled",
    "swatchCategoryType",
    "brand",
    "ageRestriction",
    "isInStoreOnly",
    "isMadeToMeasure",
    "isBundle",
    "isProductSet",
    "promotionalFeatures",
    "features",
    "dynamicAttributes",
    "directorate",
    "futureRelease",
    "multiSku",
    "fabricByLength",
    "quickAddToBasket",
    "permanentOos"
})
public class Product {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("productId")
    private String productId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("type")
    private String type;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("title")
    private String title;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("htmlTitle")
    private String htmlTitle;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("code")
    private String code;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("averageRating")
    private Double averageRating;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("reviews")
    private Double reviews;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("price")
    private Price price;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("image")
    private String image;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("displaySpecialOffer")
    private String displaySpecialOffer;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("promoMessages")
    private PromoMessages promoMessages;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("nonPromoMessage")
    private String nonPromoMessage;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("defaultSkuId")
    private String defaultSkuId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatches")
    private List<ColorSwatch> colorSwatches = new ArrayList<ColorSwatch>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchSelected")
    private Double colorSwatchSelected;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorWheelMessage")
    private String colorWheelMessage;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("outOfStock")
    private Boolean outOfStock;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("emailMeWhenAvailable")
    private Boolean emailMeWhenAvailable;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("availabilityMessage")
    private String availabilityMessage;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("compare")
    private Boolean compare;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("fabric")
    private String fabric;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("swatchAvailable")
    private Boolean swatchAvailable;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("categoryQuickViewEnabled")
    private Boolean categoryQuickViewEnabled;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("swatchCategoryType")
    private String swatchCategoryType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("brand")
    private String brand;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("ageRestriction")
    private Double ageRestriction;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isInStoreOnly")
    private Boolean isInStoreOnly;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isMadeToMeasure")
    private Boolean isMadeToMeasure;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isBundle")
    private Boolean isBundle;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isProductSet")
    private Boolean isProductSet;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("promotionalFeatures")
    private List<Object> promotionalFeatures = new ArrayList<Object>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("features")
    private List<Object> features = new ArrayList<Object>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("dynamicAttributes")
    private DynamicAttributes dynamicAttributes;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("directorate")
    private String directorate;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("futureRelease")
    private Boolean futureRelease;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("multiSku")
    private Boolean multiSku;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("fabricByLength")
    private Boolean fabricByLength;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("quickAddToBasket")
    private QuickAddToBasket quickAddToBasket;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("permanentOos")
    private Boolean permanentOos;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Product() {
    }

    /**
     *
     * @param isBundle
     * @param compare
     * @param promotionalFeatures
     * @param dynamicAttributes
     * @param code
     * @param categoryQuickViewEnabled
     * @param defaultSkuId
     * @param colorWheelMessage
     * @param displaySpecialOffer
     * @param type
     * @param title
     * @param features
     * @param htmlTitle
     * @param reviews
     * @param price
     * @param swatchCategoryType
     * @param averageRating
     * @param directorate
     * @param outOfStock
     * @param quickAddToBasket
     * @param isProductSet
     * @param multiSku
     * @param brand
     * @param availabilityMessage
     * @param image
     * @param isMadeToMeasure
     * @param colorSwatchSelected
     * @param permanentOos
     * @param promoMessages
     * @param productId
     * @param futureRelease
     * @param swatchAvailable
     * @param isInStoreOnly
     * @param fabricByLength
     * @param emailMeWhenAvailable
     * @param colorSwatches
     * @param fabric
     * @param ageRestriction
     * @param nonPromoMessage
     */
    public Product(String productId, String type, String title, String htmlTitle, String code, Double averageRating, Double reviews, Price price, String image, String displaySpecialOffer, PromoMessages promoMessages, String nonPromoMessage, String defaultSkuId, List<ColorSwatch> colorSwatches, Double colorSwatchSelected, String colorWheelMessage, Boolean outOfStock, Boolean emailMeWhenAvailable, String availabilityMessage, Boolean compare, String fabric, Boolean swatchAvailable, Boolean categoryQuickViewEnabled, String swatchCategoryType, String brand, Double ageRestriction, Boolean isInStoreOnly, Boolean isMadeToMeasure, Boolean isBundle, Boolean isProductSet, List<Object> promotionalFeatures, List<Object> features, DynamicAttributes dynamicAttributes, String directorate, Boolean futureRelease, Boolean multiSku, Boolean fabricByLength, QuickAddToBasket quickAddToBasket, Boolean permanentOos) {
        super();
        this.productId = productId;
        this.type = type;
        this.title = title;
        this.htmlTitle = htmlTitle;
        this.code = code;
        this.averageRating = averageRating;
        this.reviews = reviews;
        this.price = price;
        this.image = image;
        this.displaySpecialOffer = displaySpecialOffer;
        this.promoMessages = promoMessages;
        this.nonPromoMessage = nonPromoMessage;
        this.defaultSkuId = defaultSkuId;
        this.colorSwatches = colorSwatches;
        this.colorSwatchSelected = colorSwatchSelected;
        this.colorWheelMessage = colorWheelMessage;
        this.outOfStock = outOfStock;
        this.emailMeWhenAvailable = emailMeWhenAvailable;
        this.availabilityMessage = availabilityMessage;
        this.compare = compare;
        this.fabric = fabric;
        this.swatchAvailable = swatchAvailable;
        this.categoryQuickViewEnabled = categoryQuickViewEnabled;
        this.swatchCategoryType = swatchCategoryType;
        this.brand = brand;
        this.ageRestriction = ageRestriction;
        this.isInStoreOnly = isInStoreOnly;
        this.isMadeToMeasure = isMadeToMeasure;
        this.isBundle = isBundle;
        this.isProductSet = isProductSet;
        this.promotionalFeatures = promotionalFeatures;
        this.features = features;
        this.dynamicAttributes = dynamicAttributes;
        this.directorate = directorate;
        this.futureRelease = futureRelease;
        this.multiSku = multiSku;
        this.fabricByLength = fabricByLength;
        this.quickAddToBasket = quickAddToBasket;
        this.permanentOos = permanentOos;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("productId")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product withProductId(String productId) {
        this.productId = productId;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Product withType(String type) {
        this.type = type;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Product withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("htmlTitle")
    public String getHtmlTitle() {
        return htmlTitle;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("htmlTitle")
    public void setHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
    }

    public Product withHtmlTitle(String htmlTitle) {
        this.htmlTitle = htmlTitle;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public Product withCode(String code) {
        this.code = code;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("averageRating")
    public Double getAverageRating() {
        return averageRating;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("averageRating")
    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Product withAverageRating(Double averageRating) {
        this.averageRating = averageRating;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("reviews")
    public Double getReviews() {
        return reviews;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("reviews")
    public void setReviews(Double reviews) {
        this.reviews = reviews;
    }

    public Product withReviews(Double reviews) {
        this.reviews = reviews;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("price")
    public Price getPrice() {
        return price;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("price")
    public void setPrice(Price price) {
        this.price = price;
    }

    public Product withPrice(Price price) {
        this.price = price;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    public Product withImage(String image) {
        this.image = image;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("displaySpecialOffer")
    public String getDisplaySpecialOffer() {
        return displaySpecialOffer;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("displaySpecialOffer")
    public void setDisplaySpecialOffer(String displaySpecialOffer) {
        this.displaySpecialOffer = displaySpecialOffer;
    }

    public Product withDisplaySpecialOffer(String displaySpecialOffer) {
        this.displaySpecialOffer = displaySpecialOffer;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("promoMessages")
    public PromoMessages getPromoMessages() {
        return promoMessages;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("promoMessages")
    public void setPromoMessages(PromoMessages promoMessages) {
        this.promoMessages = promoMessages;
    }

    public Product withPromoMessages(PromoMessages promoMessages) {
        this.promoMessages = promoMessages;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("nonPromoMessage")
    public String getNonPromoMessage() {
        return nonPromoMessage;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("nonPromoMessage")
    public void setNonPromoMessage(String nonPromoMessage) {
        this.nonPromoMessage = nonPromoMessage;
    }

    public Product withNonPromoMessage(String nonPromoMessage) {
        this.nonPromoMessage = nonPromoMessage;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("defaultSkuId")
    public String getDefaultSkuId() {
        return defaultSkuId;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("defaultSkuId")
    public void setDefaultSkuId(String defaultSkuId) {
        this.defaultSkuId = defaultSkuId;
    }

    public Product withDefaultSkuId(String defaultSkuId) {
        this.defaultSkuId = defaultSkuId;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorSwatches")
    public List<ColorSwatch> getColorSwatches() {
        return colorSwatches;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorSwatches")
    public void setColorSwatches(List<ColorSwatch> colorSwatches) {
        this.colorSwatches = colorSwatches;
    }

    public Product withColorSwatches(List<ColorSwatch> colorSwatches) {
        this.colorSwatches = colorSwatches;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorSwatchSelected")
    public Double getColorSwatchSelected() {
        return colorSwatchSelected;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorSwatchSelected")
    public void setColorSwatchSelected(Double colorSwatchSelected) {
        this.colorSwatchSelected = colorSwatchSelected;
    }

    public Product withColorSwatchSelected(Double colorSwatchSelected) {
        this.colorSwatchSelected = colorSwatchSelected;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorWheelMessage")
    public String getColorWheelMessage() {
        return colorWheelMessage;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("colorWheelMessage")
    public void setColorWheelMessage(String colorWheelMessage) {
        this.colorWheelMessage = colorWheelMessage;
    }

    public Product withColorWheelMessage(String colorWheelMessage) {
        this.colorWheelMessage = colorWheelMessage;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("outOfStock")
    public Boolean getOutOfStock() {
        return outOfStock;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("outOfStock")
    public void setOutOfStock(Boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    public Product withOutOfStock(Boolean outOfStock) {
        this.outOfStock = outOfStock;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("emailMeWhenAvailable")
    public Boolean getEmailMeWhenAvailable() {
        return emailMeWhenAvailable;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("emailMeWhenAvailable")
    public void setEmailMeWhenAvailable(Boolean emailMeWhenAvailable) {
        this.emailMeWhenAvailable = emailMeWhenAvailable;
    }

    public Product withEmailMeWhenAvailable(Boolean emailMeWhenAvailable) {
        this.emailMeWhenAvailable = emailMeWhenAvailable;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("availabilityMessage")
    public String getAvailabilityMessage() {
        return availabilityMessage;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("availabilityMessage")
    public void setAvailabilityMessage(String availabilityMessage) {
        this.availabilityMessage = availabilityMessage;
    }

    public Product withAvailabilityMessage(String availabilityMessage) {
        this.availabilityMessage = availabilityMessage;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("compare")
    public Boolean getCompare() {
        return compare;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("compare")
    public void setCompare(Boolean compare) {
        this.compare = compare;
    }

    public Product withCompare(Boolean compare) {
        this.compare = compare;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("fabric")
    public String getFabric() {
        return fabric;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("fabric")
    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public Product withFabric(String fabric) {
        this.fabric = fabric;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("swatchAvailable")
    public Boolean getSwatchAvailable() {
        return swatchAvailable;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("swatchAvailable")
    public void setSwatchAvailable(Boolean swatchAvailable) {
        this.swatchAvailable = swatchAvailable;
    }

    public Product withSwatchAvailable(Boolean swatchAvailable) {
        this.swatchAvailable = swatchAvailable;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("categoryQuickViewEnabled")
    public Boolean getCategoryQuickViewEnabled() {
        return categoryQuickViewEnabled;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("categoryQuickViewEnabled")
    public void setCategoryQuickViewEnabled(Boolean categoryQuickViewEnabled) {
        this.categoryQuickViewEnabled = categoryQuickViewEnabled;
    }

    public Product withCategoryQuickViewEnabled(Boolean categoryQuickViewEnabled) {
        this.categoryQuickViewEnabled = categoryQuickViewEnabled;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("swatchCategoryType")
    public String getSwatchCategoryType() {
        return swatchCategoryType;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("swatchCategoryType")
    public void setSwatchCategoryType(String swatchCategoryType) {
        this.swatchCategoryType = swatchCategoryType;
    }

    public Product withSwatchCategoryType(String swatchCategoryType) {
        this.swatchCategoryType = swatchCategoryType;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("brand")
    public String getBrand() {
        return brand;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("brand")
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Product withBrand(String brand) {
        this.brand = brand;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("ageRestriction")
    public Double getAgeRestriction() {
        return ageRestriction;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("ageRestriction")
    public void setAgeRestriction(Double ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    public Product withAgeRestriction(Double ageRestriction) {
        this.ageRestriction = ageRestriction;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isInStoreOnly")
    public Boolean getIsInStoreOnly() {
        return isInStoreOnly;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isInStoreOnly")
    public void setIsInStoreOnly(Boolean isInStoreOnly) {
        this.isInStoreOnly = isInStoreOnly;
    }

    public Product withIsInStoreOnly(Boolean isInStoreOnly) {
        this.isInStoreOnly = isInStoreOnly;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isMadeToMeasure")
    public Boolean getIsMadeToMeasure() {
        return isMadeToMeasure;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isMadeToMeasure")
    public void setIsMadeToMeasure(Boolean isMadeToMeasure) {
        this.isMadeToMeasure = isMadeToMeasure;
    }

    public Product withIsMadeToMeasure(Boolean isMadeToMeasure) {
        this.isMadeToMeasure = isMadeToMeasure;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isBundle")
    public Boolean getIsBundle() {
        return isBundle;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isBundle")
    public void setIsBundle(Boolean isBundle) {
        this.isBundle = isBundle;
    }

    public Product withIsBundle(Boolean isBundle) {
        this.isBundle = isBundle;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isProductSet")
    public Boolean getIsProductSet() {
        return isProductSet;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isProductSet")
    public void setIsProductSet(Boolean isProductSet) {
        this.isProductSet = isProductSet;
    }

    public Product withIsProductSet(Boolean isProductSet) {
        this.isProductSet = isProductSet;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("promotionalFeatures")
    public List<Object> getPromotionalFeatures() {
        return promotionalFeatures;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("promotionalFeatures")
    public void setPromotionalFeatures(List<Object> promotionalFeatures) {
        this.promotionalFeatures = promotionalFeatures;
    }

    public Product withPromotionalFeatures(List<Object> promotionalFeatures) {
        this.promotionalFeatures = promotionalFeatures;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("features")
    public List<Object> getFeatures() {
        return features;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("features")
    public void setFeatures(List<Object> features) {
        this.features = features;
    }

    public Product withFeatures(List<Object> features) {
        this.features = features;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("dynamicAttributes")
    public DynamicAttributes getDynamicAttributes() {
        return dynamicAttributes;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("dynamicAttributes")
    public void setDynamicAttributes(DynamicAttributes dynamicAttributes) {
        this.dynamicAttributes = dynamicAttributes;
    }

    public Product withDynamicAttributes(DynamicAttributes dynamicAttributes) {
        this.dynamicAttributes = dynamicAttributes;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("directorate")
    public String getDirectorate() {
        return directorate;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("directorate")
    public void setDirectorate(String directorate) {
        this.directorate = directorate;
    }

    public Product withDirectorate(String directorate) {
        this.directorate = directorate;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("futureRelease")
    public Boolean getFutureRelease() {
        return futureRelease;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("futureRelease")
    public void setFutureRelease(Boolean futureRelease) {
        this.futureRelease = futureRelease;
    }

    public Product withFutureRelease(Boolean futureRelease) {
        this.futureRelease = futureRelease;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("multiSku")
    public Boolean getMultiSku() {
        return multiSku;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("multiSku")
    public void setMultiSku(Boolean multiSku) {
        this.multiSku = multiSku;
    }

    public Product withMultiSku(Boolean multiSku) {
        this.multiSku = multiSku;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("fabricByLength")
    public Boolean getFabricByLength() {
        return fabricByLength;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("fabricByLength")
    public void setFabricByLength(Boolean fabricByLength) {
        this.fabricByLength = fabricByLength;
    }

    public Product withFabricByLength(Boolean fabricByLength) {
        this.fabricByLength = fabricByLength;
        return this;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("quickAddToBasket")
    public QuickAddToBasket getQuickAddToBasket() {
        return quickAddToBasket;
    }

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("quickAddToBasket")
    public void setQuickAddToBasket(QuickAddToBasket quickAddToBasket) {
        this.quickAddToBasket = quickAddToBasket;
    }

    public Product withQuickAddToBasket(QuickAddToBasket quickAddToBasket) {
        this.quickAddToBasket = quickAddToBasket;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("permanentOos")
    public Boolean getPermanentOos() {
        return permanentOos;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("permanentOos")
    public void setPermanentOos(Boolean permanentOos) {
        this.permanentOos = permanentOos;
    }

    public Product withPermanentOos(Boolean permanentOos) {
        this.permanentOos = permanentOos;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Product withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Product.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("productId");
        sb.append('=');
        sb.append(((this.productId == null)?"<null>":this.productId));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null)?"<null>":this.type));
        sb.append(',');
        sb.append("title");
        sb.append('=');
        sb.append(((this.title == null)?"<null>":this.title));
        sb.append(',');
        sb.append("htmlTitle");
        sb.append('=');
        sb.append(((this.htmlTitle == null)?"<null>":this.htmlTitle));
        sb.append(',');
        sb.append("code");
        sb.append('=');
        sb.append(((this.code == null)?"<null>":this.code));
        sb.append(',');
        sb.append("averageRating");
        sb.append('=');
        sb.append(((this.averageRating == null)?"<null>":this.averageRating));
        sb.append(',');
        sb.append("reviews");
        sb.append('=');
        sb.append(((this.reviews == null)?"<null>":this.reviews));
        sb.append(',');
        sb.append("price");
        sb.append('=');
        sb.append(((this.price == null)?"<null>":this.price));
        sb.append(',');
        sb.append("image");
        sb.append('=');
        sb.append(((this.image == null)?"<null>":this.image));
        sb.append(',');
        sb.append("displaySpecialOffer");
        sb.append('=');
        sb.append(((this.displaySpecialOffer == null)?"<null>":this.displaySpecialOffer));
        sb.append(',');
        sb.append("promoMessages");
        sb.append('=');
        sb.append(((this.promoMessages == null)?"<null>":this.promoMessages));
        sb.append(',');
        sb.append("nonPromoMessage");
        sb.append('=');
        sb.append(((this.nonPromoMessage == null)?"<null>":this.nonPromoMessage));
        sb.append(',');
        sb.append("defaultSkuId");
        sb.append('=');
        sb.append(((this.defaultSkuId == null)?"<null>":this.defaultSkuId));
        sb.append(',');
        sb.append("colorSwatches");
        sb.append('=');
        sb.append(((this.colorSwatches == null)?"<null>":this.colorSwatches));
        sb.append(',');
        sb.append("colorSwatchSelected");
        sb.append('=');
        sb.append(((this.colorSwatchSelected == null)?"<null>":this.colorSwatchSelected));
        sb.append(',');
        sb.append("colorWheelMessage");
        sb.append('=');
        sb.append(((this.colorWheelMessage == null)?"<null>":this.colorWheelMessage));
        sb.append(',');
        sb.append("outOfStock");
        sb.append('=');
        sb.append(((this.outOfStock == null)?"<null>":this.outOfStock));
        sb.append(',');
        sb.append("emailMeWhenAvailable");
        sb.append('=');
        sb.append(((this.emailMeWhenAvailable == null)?"<null>":this.emailMeWhenAvailable));
        sb.append(',');
        sb.append("availabilityMessage");
        sb.append('=');
        sb.append(((this.availabilityMessage == null)?"<null>":this.availabilityMessage));
        sb.append(',');
        sb.append("compare");
        sb.append('=');
        sb.append(((this.compare == null)?"<null>":this.compare));
        sb.append(',');
        sb.append("fabric");
        sb.append('=');
        sb.append(((this.fabric == null)?"<null>":this.fabric));
        sb.append(',');
        sb.append("swatchAvailable");
        sb.append('=');
        sb.append(((this.swatchAvailable == null)?"<null>":this.swatchAvailable));
        sb.append(',');
        sb.append("categoryQuickViewEnabled");
        sb.append('=');
        sb.append(((this.categoryQuickViewEnabled == null)?"<null>":this.categoryQuickViewEnabled));
        sb.append(',');
        sb.append("swatchCategoryType");
        sb.append('=');
        sb.append(((this.swatchCategoryType == null)?"<null>":this.swatchCategoryType));
        sb.append(',');
        sb.append("brand");
        sb.append('=');
        sb.append(((this.brand == null)?"<null>":this.brand));
        sb.append(',');
        sb.append("ageRestriction");
        sb.append('=');
        sb.append(((this.ageRestriction == null)?"<null>":this.ageRestriction));
        sb.append(',');
        sb.append("isInStoreOnly");
        sb.append('=');
        sb.append(((this.isInStoreOnly == null)?"<null>":this.isInStoreOnly));
        sb.append(',');
        sb.append("isMadeToMeasure");
        sb.append('=');
        sb.append(((this.isMadeToMeasure == null)?"<null>":this.isMadeToMeasure));
        sb.append(',');
        sb.append("isBundle");
        sb.append('=');
        sb.append(((this.isBundle == null)?"<null>":this.isBundle));
        sb.append(',');
        sb.append("isProductSet");
        sb.append('=');
        sb.append(((this.isProductSet == null)?"<null>":this.isProductSet));
        sb.append(',');
        sb.append("promotionalFeatures");
        sb.append('=');
        sb.append(((this.promotionalFeatures == null)?"<null>":this.promotionalFeatures));
        sb.append(',');
        sb.append("features");
        sb.append('=');
        sb.append(((this.features == null)?"<null>":this.features));
        sb.append(',');
        sb.append("dynamicAttributes");
        sb.append('=');
        sb.append(((this.dynamicAttributes == null)?"<null>":this.dynamicAttributes));
        sb.append(',');
        sb.append("directorate");
        sb.append('=');
        sb.append(((this.directorate == null)?"<null>":this.directorate));
        sb.append(',');
        sb.append("futureRelease");
        sb.append('=');
        sb.append(((this.futureRelease == null)?"<null>":this.futureRelease));
        sb.append(',');
        sb.append("multiSku");
        sb.append('=');
        sb.append(((this.multiSku == null)?"<null>":this.multiSku));
        sb.append(',');
        sb.append("fabricByLength");
        sb.append('=');
        sb.append(((this.fabricByLength == null)?"<null>":this.fabricByLength));
        sb.append(',');
        sb.append("quickAddToBasket");
        sb.append('=');
        sb.append(((this.quickAddToBasket == null)?"<null>":this.quickAddToBasket));
        sb.append(',');
        sb.append("permanentOos");
        sb.append('=');
        sb.append(((this.permanentOos == null)?"<null>":this.permanentOos));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.isBundle == null)? 0 :this.isBundle.hashCode()));
        result = ((result* 31)+((this.compare == null)? 0 :this.compare.hashCode()));
        result = ((result* 31)+((this.promotionalFeatures == null)? 0 :this.promotionalFeatures.hashCode()));
        result = ((result* 31)+((this.dynamicAttributes == null)? 0 :this.dynamicAttributes.hashCode()));
        result = ((result* 31)+((this.code == null)? 0 :this.code.hashCode()));
        result = ((result* 31)+((this.categoryQuickViewEnabled == null)? 0 :this.categoryQuickViewEnabled.hashCode()));
        result = ((result* 31)+((this.defaultSkuId == null)? 0 :this.defaultSkuId.hashCode()));
        result = ((result* 31)+((this.colorWheelMessage == null)? 0 :this.colorWheelMessage.hashCode()));
        result = ((result* 31)+((this.displaySpecialOffer == null)? 0 :this.displaySpecialOffer.hashCode()));
        result = ((result* 31)+((this.type == null)? 0 :this.type.hashCode()));
        result = ((result* 31)+((this.title == null)? 0 :this.title.hashCode()));
        result = ((result* 31)+((this.features == null)? 0 :this.features.hashCode()));
        result = ((result* 31)+((this.htmlTitle == null)? 0 :this.htmlTitle.hashCode()));
        result = ((result* 31)+((this.reviews == null)? 0 :this.reviews.hashCode()));
        result = ((result* 31)+((this.price == null)? 0 :this.price.hashCode()));
        result = ((result* 31)+((this.swatchCategoryType == null)? 0 :this.swatchCategoryType.hashCode()));
        result = ((result* 31)+((this.averageRating == null)? 0 :this.averageRating.hashCode()));
        result = ((result* 31)+((this.directorate == null)? 0 :this.directorate.hashCode()));
        result = ((result* 31)+((this.outOfStock == null)? 0 :this.outOfStock.hashCode()));
        result = ((result* 31)+((this.quickAddToBasket == null)? 0 :this.quickAddToBasket.hashCode()));
        result = ((result* 31)+((this.isProductSet == null)? 0 :this.isProductSet.hashCode()));
        result = ((result* 31)+((this.multiSku == null)? 0 :this.multiSku.hashCode()));
        result = ((result* 31)+((this.brand == null)? 0 :this.brand.hashCode()));
        result = ((result* 31)+((this.availabilityMessage == null)? 0 :this.availabilityMessage.hashCode()));
        result = ((result* 31)+((this.image == null)? 0 :this.image.hashCode()));
        result = ((result* 31)+((this.isMadeToMeasure == null)? 0 :this.isMadeToMeasure.hashCode()));
        result = ((result* 31)+((this.colorSwatchSelected == null)? 0 :this.colorSwatchSelected.hashCode()));
        result = ((result* 31)+((this.permanentOos == null)? 0 :this.permanentOos.hashCode()));
        result = ((result* 31)+((this.promoMessages == null)? 0 :this.promoMessages.hashCode()));
        result = ((result* 31)+((this.productId == null)? 0 :this.productId.hashCode()));
        result = ((result* 31)+((this.futureRelease == null)? 0 :this.futureRelease.hashCode()));
        result = ((result* 31)+((this.swatchAvailable == null)? 0 :this.swatchAvailable.hashCode()));
        result = ((result* 31)+((this.isInStoreOnly == null)? 0 :this.isInStoreOnly.hashCode()));
        result = ((result* 31)+((this.fabricByLength == null)? 0 :this.fabricByLength.hashCode()));
        result = ((result* 31)+((this.emailMeWhenAvailable == null)? 0 :this.emailMeWhenAvailable.hashCode()));
        result = ((result* 31)+((this.colorSwatches == null)? 0 :this.colorSwatches.hashCode()));
        result = ((result* 31)+((this.fabric == null)? 0 :this.fabric.hashCode()));
        result = ((result* 31)+((this.ageRestriction == null)? 0 :this.ageRestriction.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.nonPromoMessage == null)? 0 :this.nonPromoMessage.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Product) == false) {
            return false;
        }
        Product rhs = ((Product) other);
        return (((((((((((((((((((((((((((((((((((((((((this.isBundle == rhs.isBundle)||((this.isBundle!= null)&&this.isBundle.equals(rhs.isBundle)))&&((this.compare == rhs.compare)||((this.compare!= null)&&this.compare.equals(rhs.compare))))&&((this.promotionalFeatures == rhs.promotionalFeatures)||((this.promotionalFeatures!= null)&&this.promotionalFeatures.equals(rhs.promotionalFeatures))))&&((this.dynamicAttributes == rhs.dynamicAttributes)||((this.dynamicAttributes!= null)&&this.dynamicAttributes.equals(rhs.dynamicAttributes))))&&((this.code == rhs.code)||((this.code!= null)&&this.code.equals(rhs.code))))&&((this.categoryQuickViewEnabled == rhs.categoryQuickViewEnabled)||((this.categoryQuickViewEnabled!= null)&&this.categoryQuickViewEnabled.equals(rhs.categoryQuickViewEnabled))))&&((this.defaultSkuId == rhs.defaultSkuId)||((this.defaultSkuId!= null)&&this.defaultSkuId.equals(rhs.defaultSkuId))))&&((this.colorWheelMessage == rhs.colorWheelMessage)||((this.colorWheelMessage!= null)&&this.colorWheelMessage.equals(rhs.colorWheelMessage))))&&((this.displaySpecialOffer == rhs.displaySpecialOffer)||((this.displaySpecialOffer!= null)&&this.displaySpecialOffer.equals(rhs.displaySpecialOffer))))&&((this.type == rhs.type)||((this.type!= null)&&this.type.equals(rhs.type))))&&((this.title == rhs.title)||((this.title!= null)&&this.title.equals(rhs.title))))&&((this.features == rhs.features)||((this.features!= null)&&this.features.equals(rhs.features))))&&((this.htmlTitle == rhs.htmlTitle)||((this.htmlTitle!= null)&&this.htmlTitle.equals(rhs.htmlTitle))))&&((this.reviews == rhs.reviews)||((this.reviews!= null)&&this.reviews.equals(rhs.reviews))))&&((this.price == rhs.price)||((this.price!= null)&&this.price.equals(rhs.price))))&&((this.swatchCategoryType == rhs.swatchCategoryType)||((this.swatchCategoryType!= null)&&this.swatchCategoryType.equals(rhs.swatchCategoryType))))&&((this.averageRating == rhs.averageRating)||((this.averageRating!= null)&&this.averageRating.equals(rhs.averageRating))))&&((this.directorate == rhs.directorate)||((this.directorate!= null)&&this.directorate.equals(rhs.directorate))))&&((this.outOfStock == rhs.outOfStock)||((this.outOfStock!= null)&&this.outOfStock.equals(rhs.outOfStock))))&&((this.quickAddToBasket == rhs.quickAddToBasket)||((this.quickAddToBasket!= null)&&this.quickAddToBasket.equals(rhs.quickAddToBasket))))&&((this.isProductSet == rhs.isProductSet)||((this.isProductSet!= null)&&this.isProductSet.equals(rhs.isProductSet))))&&((this.multiSku == rhs.multiSku)||((this.multiSku!= null)&&this.multiSku.equals(rhs.multiSku))))&&((this.brand == rhs.brand)||((this.brand!= null)&&this.brand.equals(rhs.brand))))&&((this.availabilityMessage == rhs.availabilityMessage)||((this.availabilityMessage!= null)&&this.availabilityMessage.equals(rhs.availabilityMessage))))&&((this.image == rhs.image)||((this.image!= null)&&this.image.equals(rhs.image))))&&((this.isMadeToMeasure == rhs.isMadeToMeasure)||((this.isMadeToMeasure!= null)&&this.isMadeToMeasure.equals(rhs.isMadeToMeasure))))&&((this.colorSwatchSelected == rhs.colorSwatchSelected)||((this.colorSwatchSelected!= null)&&this.colorSwatchSelected.equals(rhs.colorSwatchSelected))))&&((this.permanentOos == rhs.permanentOos)||((this.permanentOos!= null)&&this.permanentOos.equals(rhs.permanentOos))))&&((this.promoMessages == rhs.promoMessages)||((this.promoMessages!= null)&&this.promoMessages.equals(rhs.promoMessages))))&&((this.productId == rhs.productId)||((this.productId!= null)&&this.productId.equals(rhs.productId))))&&((this.futureRelease == rhs.futureRelease)||((this.futureRelease!= null)&&this.futureRelease.equals(rhs.futureRelease))))&&((this.swatchAvailable == rhs.swatchAvailable)||((this.swatchAvailable!= null)&&this.swatchAvailable.equals(rhs.swatchAvailable))))&&((this.isInStoreOnly == rhs.isInStoreOnly)||((this.isInStoreOnly!= null)&&this.isInStoreOnly.equals(rhs.isInStoreOnly))))&&((this.fabricByLength == rhs.fabricByLength)||((this.fabricByLength!= null)&&this.fabricByLength.equals(rhs.fabricByLength))))&&((this.emailMeWhenAvailable == rhs.emailMeWhenAvailable)||((this.emailMeWhenAvailable!= null)&&this.emailMeWhenAvailable.equals(rhs.emailMeWhenAvailable))))&&((this.colorSwatches == rhs.colorSwatches)||((this.colorSwatches!= null)&&this.colorSwatches.equals(rhs.colorSwatches))))&&((this.fabric == rhs.fabric)||((this.fabric!= null)&&this.fabric.equals(rhs.fabric))))&&((this.ageRestriction == rhs.ageRestriction)||((this.ageRestriction!= null)&&this.ageRestriction.equals(rhs.ageRestriction))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.nonPromoMessage == rhs.nonPromoMessage)||((this.nonPromoMessage!= null)&&this.nonPromoMessage.equals(rhs.nonPromoMessage))));
    }

}
