
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "facetId",
    "label",
    "qty",
    "color",
    "colorSwatchUrl",
    "isSelected"
})
public class Detail {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("facetId")
    private String facetId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    private String label;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("qty")
    private String qty;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    private String color;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    private String colorSwatchUrl;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isSelected")
    private String isSelected;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Detail() {
    }

    /**
     * 
     * @param colorSwatchUrl
     * @param color
     * @param facetId
     * @param qty
     * @param isSelected
     * @param label
     */
    public Detail(String facetId, String label, String qty, String color, String colorSwatchUrl, String isSelected) {
        super();
        this.facetId = facetId;
        this.label = label;
        this.qty = qty;
        this.color = color;
        this.colorSwatchUrl = colorSwatchUrl;
        this.isSelected = isSelected;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("facetId")
    public String getFacetId() {
        return facetId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("facetId")
    public void setFacetId(String facetId) {
        this.facetId = facetId;
    }

    public Detail withFacetId(String facetId) {
        this.facetId = facetId;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    public Detail withLabel(String label) {
        this.label = label;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("qty")
    public String getQty() {
        return qty;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("qty")
    public void setQty(String qty) {
        this.qty = qty;
    }

    public Detail withQty(String qty) {
        this.qty = qty;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    public Detail withColor(String color) {
        this.color = color;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    public String getColorSwatchUrl() {
        return colorSwatchUrl;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("colorSwatchUrl")
    public void setColorSwatchUrl(String colorSwatchUrl) {
        this.colorSwatchUrl = colorSwatchUrl;
    }

    public Detail withColorSwatchUrl(String colorSwatchUrl) {
        this.colorSwatchUrl = colorSwatchUrl;
        return this;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isSelected")
    public String getIsSelected() {
        return isSelected;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isSelected")
    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public Detail withIsSelected(String isSelected) {
        this.isSelected = isSelected;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Detail withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Detail.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("facetId");
        sb.append('=');
        sb.append(((this.facetId == null)?"<null>":this.facetId));
        sb.append(',');
        sb.append("label");
        sb.append('=');
        sb.append(((this.label == null)?"<null>":this.label));
        sb.append(',');
        sb.append("qty");
        sb.append('=');
        sb.append(((this.qty == null)?"<null>":this.qty));
        sb.append(',');
        sb.append("color");
        sb.append('=');
        sb.append(((this.color == null)?"<null>":this.color));
        sb.append(',');
        sb.append("colorSwatchUrl");
        sb.append('=');
        sb.append(((this.colorSwatchUrl == null)?"<null>":this.colorSwatchUrl));
        sb.append(',');
        sb.append("isSelected");
        sb.append('=');
        sb.append(((this.isSelected == null)?"<null>":this.isSelected));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.colorSwatchUrl == null)? 0 :this.colorSwatchUrl.hashCode()));
        result = ((result* 31)+((this.color == null)? 0 :this.color.hashCode()));
        result = ((result* 31)+((this.facetId == null)? 0 :this.facetId.hashCode()));
        result = ((result* 31)+((this.qty == null)? 0 :this.qty.hashCode()));
        result = ((result* 31)+((this.isSelected == null)? 0 :this.isSelected.hashCode()));
        result = ((result* 31)+((this.label == null)? 0 :this.label.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Detail) == false) {
            return false;
        }
        Detail rhs = ((Detail) other);
        return ((((((((this.colorSwatchUrl == rhs.colorSwatchUrl)||((this.colorSwatchUrl!= null)&&this.colorSwatchUrl.equals(rhs.colorSwatchUrl)))&&((this.color == rhs.color)||((this.color!= null)&&this.color.equals(rhs.color))))&&((this.facetId == rhs.facetId)||((this.facetId!= null)&&this.facetId.equals(rhs.facetId))))&&((this.qty == rhs.qty)||((this.qty!= null)&&this.qty.equals(rhs.qty))))&&((this.isSelected == rhs.isSelected)||((this.isSelected!= null)&&this.isSelected.equals(rhs.isSelected))))&&((this.label == rhs.label)||((this.label!= null)&&this.label.equals(rhs.label))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }

}
