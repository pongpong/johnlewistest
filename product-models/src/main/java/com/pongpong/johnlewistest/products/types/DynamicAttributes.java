
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dressshape",
    "typeofpattern",
    "fabric",
    "dressbyoccasion",
    "mensshoefastening",
    "womensspecialistfit",
    "luxuryfabric"
})
public class DynamicAttributes {

    @JsonProperty("dressshape")
    private String dressshape;
    @JsonProperty("typeofpattern")
    private String typeofpattern;
    @JsonProperty("fabric")
    private String fabric;
    @JsonProperty("dressbyoccasion")
    private String dressbyoccasion;
    @JsonProperty("mensshoefastening")
    private String mensshoefastening;
    @JsonProperty("womensspecialistfit")
    private String womensspecialistfit;
    @JsonProperty("luxuryfabric")
    private String luxuryfabric;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public DynamicAttributes() {
    }

    /**
     * 
     * @param womensspecialistfit
     * @param luxuryfabric
     * @param dressshape
     * @param typeofpattern
     * @param fabric
     * @param dressbyoccasion
     * @param mensshoefastening
     */
    public DynamicAttributes(String dressshape, String typeofpattern, String fabric, String dressbyoccasion, String mensshoefastening, String womensspecialistfit, String luxuryfabric) {
        super();
        this.dressshape = dressshape;
        this.typeofpattern = typeofpattern;
        this.fabric = fabric;
        this.dressbyoccasion = dressbyoccasion;
        this.mensshoefastening = mensshoefastening;
        this.womensspecialistfit = womensspecialistfit;
        this.luxuryfabric = luxuryfabric;
    }

    @JsonProperty("dressshape")
    public String getDressshape() {
        return dressshape;
    }

    @JsonProperty("dressshape")
    public void setDressshape(String dressshape) {
        this.dressshape = dressshape;
    }

    public DynamicAttributes withDressshape(String dressshape) {
        this.dressshape = dressshape;
        return this;
    }

    @JsonProperty("typeofpattern")
    public String getTypeofpattern() {
        return typeofpattern;
    }

    @JsonProperty("typeofpattern")
    public void setTypeofpattern(String typeofpattern) {
        this.typeofpattern = typeofpattern;
    }

    public DynamicAttributes withTypeofpattern(String typeofpattern) {
        this.typeofpattern = typeofpattern;
        return this;
    }

    @JsonProperty("fabric")
    public String getFabric() {
        return fabric;
    }

    @JsonProperty("fabric")
    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public DynamicAttributes withFabric(String fabric) {
        this.fabric = fabric;
        return this;
    }

    @JsonProperty("dressbyoccasion")
    public String getDressbyoccasion() {
        return dressbyoccasion;
    }

    @JsonProperty("dressbyoccasion")
    public void setDressbyoccasion(String dressbyoccasion) {
        this.dressbyoccasion = dressbyoccasion;
    }

    public DynamicAttributes withDressbyoccasion(String dressbyoccasion) {
        this.dressbyoccasion = dressbyoccasion;
        return this;
    }

    @JsonProperty("mensshoefastening")
    public String getMensshoefastening() {
        return mensshoefastening;
    }

    @JsonProperty("mensshoefastening")
    public void setMensshoefastening(String mensshoefastening) {
        this.mensshoefastening = mensshoefastening;
    }

    public DynamicAttributes withMensshoefastening(String mensshoefastening) {
        this.mensshoefastening = mensshoefastening;
        return this;
    }

    @JsonProperty("womensspecialistfit")
    public String getWomensspecialistfit() {
        return womensspecialistfit;
    }

    @JsonProperty("womensspecialistfit")
    public void setWomensspecialistfit(String womensspecialistfit) {
        this.womensspecialistfit = womensspecialistfit;
    }

    public DynamicAttributes withWomensspecialistfit(String womensspecialistfit) {
        this.womensspecialistfit = womensspecialistfit;
        return this;
    }

    @JsonProperty("luxuryfabric")
    public String getLuxuryfabric() {
        return luxuryfabric;
    }

    @JsonProperty("luxuryfabric")
    public void setLuxuryfabric(String luxuryfabric) {
        this.luxuryfabric = luxuryfabric;
    }

    public DynamicAttributes withLuxuryfabric(String luxuryfabric) {
        this.luxuryfabric = luxuryfabric;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DynamicAttributes withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(DynamicAttributes.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("dressshape");
        sb.append('=');
        sb.append(((this.dressshape == null)?"<null>":this.dressshape));
        sb.append(',');
        sb.append("typeofpattern");
        sb.append('=');
        sb.append(((this.typeofpattern == null)?"<null>":this.typeofpattern));
        sb.append(',');
        sb.append("fabric");
        sb.append('=');
        sb.append(((this.fabric == null)?"<null>":this.fabric));
        sb.append(',');
        sb.append("dressbyoccasion");
        sb.append('=');
        sb.append(((this.dressbyoccasion == null)?"<null>":this.dressbyoccasion));
        sb.append(',');
        sb.append("mensshoefastening");
        sb.append('=');
        sb.append(((this.mensshoefastening == null)?"<null>":this.mensshoefastening));
        sb.append(',');
        sb.append("womensspecialistfit");
        sb.append('=');
        sb.append(((this.womensspecialistfit == null)?"<null>":this.womensspecialistfit));
        sb.append(',');
        sb.append("luxuryfabric");
        sb.append('=');
        sb.append(((this.luxuryfabric == null)?"<null>":this.luxuryfabric));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.womensspecialistfit == null)? 0 :this.womensspecialistfit.hashCode()));
        result = ((result* 31)+((this.luxuryfabric == null)? 0 :this.luxuryfabric.hashCode()));
        result = ((result* 31)+((this.dressshape == null)? 0 :this.dressshape.hashCode()));
        result = ((result* 31)+((this.typeofpattern == null)? 0 :this.typeofpattern.hashCode()));
        result = ((result* 31)+((this.fabric == null)? 0 :this.fabric.hashCode()));
        result = ((result* 31)+((this.dressbyoccasion == null)? 0 :this.dressbyoccasion.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.mensshoefastening == null)? 0 :this.mensshoefastening.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DynamicAttributes) == false) {
            return false;
        }
        DynamicAttributes rhs = ((DynamicAttributes) other);
        return (((((((((this.womensspecialistfit == rhs.womensspecialistfit)||((this.womensspecialistfit!= null)&&this.womensspecialistfit.equals(rhs.womensspecialistfit)))&&((this.luxuryfabric == rhs.luxuryfabric)||((this.luxuryfabric!= null)&&this.luxuryfabric.equals(rhs.luxuryfabric))))&&((this.dressshape == rhs.dressshape)||((this.dressshape!= null)&&this.dressshape.equals(rhs.dressshape))))&&((this.typeofpattern == rhs.typeofpattern)||((this.typeofpattern!= null)&&this.typeofpattern.equals(rhs.typeofpattern))))&&((this.fabric == rhs.fabric)||((this.fabric!= null)&&this.fabric.equals(rhs.fabric))))&&((this.dressbyoccasion == rhs.dressbyoccasion)||((this.dressbyoccasion!= null)&&this.dressbyoccasion.equals(rhs.dressbyoccasion))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.mensshoefastening == rhs.mensshoefastening)||((this.mensshoefastening!= null)&&this.mensshoefastening.equals(rhs.mensshoefastening))));
    }

}
