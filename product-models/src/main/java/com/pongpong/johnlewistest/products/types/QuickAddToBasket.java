
package com.pongpong.johnlewistest.products.types;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "showPermanentOos",
    "showMoreInfoRedirectPDP",
    "simpleAddToBasket",
    "simpleMobileEmailMe",
    "showEmailMeTriggerQV",
    "showChooseSizeTriggerQV"
})
public class QuickAddToBasket {

    @JsonProperty("showPermanentOos")
    private Boolean showPermanentOos;
    @JsonProperty("showMoreInfoRedirectPDP")
    private Boolean showMoreInfoRedirectPDP;
    @JsonProperty("simpleAddToBasket")
    private Boolean simpleAddToBasket;
    @JsonProperty("simpleMobileEmailMe")
    private Boolean simpleMobileEmailMe;
    @JsonProperty("showEmailMeTriggerQV")
    private Boolean showEmailMeTriggerQV;
    @JsonProperty("showChooseSizeTriggerQV")
    private Boolean showChooseSizeTriggerQV;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public QuickAddToBasket() {
    }

    /**
     * 
     * @param simpleAddToBasket
     * @param showEmailMeTriggerQV
     * @param showChooseSizeTriggerQV
     * @param showPermanentOos
     * @param simpleMobileEmailMe
     * @param showMoreInfoRedirectPDP
     */
    public QuickAddToBasket(Boolean showPermanentOos, Boolean showMoreInfoRedirectPDP, Boolean simpleAddToBasket, Boolean simpleMobileEmailMe, Boolean showEmailMeTriggerQV, Boolean showChooseSizeTriggerQV) {
        super();
        this.showPermanentOos = showPermanentOos;
        this.showMoreInfoRedirectPDP = showMoreInfoRedirectPDP;
        this.simpleAddToBasket = simpleAddToBasket;
        this.simpleMobileEmailMe = simpleMobileEmailMe;
        this.showEmailMeTriggerQV = showEmailMeTriggerQV;
        this.showChooseSizeTriggerQV = showChooseSizeTriggerQV;
    }

    @JsonProperty("showPermanentOos")
    public Boolean getShowPermanentOos() {
        return showPermanentOos;
    }

    @JsonProperty("showPermanentOos")
    public void setShowPermanentOos(Boolean showPermanentOos) {
        this.showPermanentOos = showPermanentOos;
    }

    public QuickAddToBasket withShowPermanentOos(Boolean showPermanentOos) {
        this.showPermanentOos = showPermanentOos;
        return this;
    }

    @JsonProperty("showMoreInfoRedirectPDP")
    public Boolean getShowMoreInfoRedirectPDP() {
        return showMoreInfoRedirectPDP;
    }

    @JsonProperty("showMoreInfoRedirectPDP")
    public void setShowMoreInfoRedirectPDP(Boolean showMoreInfoRedirectPDP) {
        this.showMoreInfoRedirectPDP = showMoreInfoRedirectPDP;
    }

    public QuickAddToBasket withShowMoreInfoRedirectPDP(Boolean showMoreInfoRedirectPDP) {
        this.showMoreInfoRedirectPDP = showMoreInfoRedirectPDP;
        return this;
    }

    @JsonProperty("simpleAddToBasket")
    public Boolean getSimpleAddToBasket() {
        return simpleAddToBasket;
    }

    @JsonProperty("simpleAddToBasket")
    public void setSimpleAddToBasket(Boolean simpleAddToBasket) {
        this.simpleAddToBasket = simpleAddToBasket;
    }

    public QuickAddToBasket withSimpleAddToBasket(Boolean simpleAddToBasket) {
        this.simpleAddToBasket = simpleAddToBasket;
        return this;
    }

    @JsonProperty("simpleMobileEmailMe")
    public Boolean getSimpleMobileEmailMe() {
        return simpleMobileEmailMe;
    }

    @JsonProperty("simpleMobileEmailMe")
    public void setSimpleMobileEmailMe(Boolean simpleMobileEmailMe) {
        this.simpleMobileEmailMe = simpleMobileEmailMe;
    }

    public QuickAddToBasket withSimpleMobileEmailMe(Boolean simpleMobileEmailMe) {
        this.simpleMobileEmailMe = simpleMobileEmailMe;
        return this;
    }

    @JsonProperty("showEmailMeTriggerQV")
    public Boolean getShowEmailMeTriggerQV() {
        return showEmailMeTriggerQV;
    }

    @JsonProperty("showEmailMeTriggerQV")
    public void setShowEmailMeTriggerQV(Boolean showEmailMeTriggerQV) {
        this.showEmailMeTriggerQV = showEmailMeTriggerQV;
    }

    public QuickAddToBasket withShowEmailMeTriggerQV(Boolean showEmailMeTriggerQV) {
        this.showEmailMeTriggerQV = showEmailMeTriggerQV;
        return this;
    }

    @JsonProperty("showChooseSizeTriggerQV")
    public Boolean getShowChooseSizeTriggerQV() {
        return showChooseSizeTriggerQV;
    }

    @JsonProperty("showChooseSizeTriggerQV")
    public void setShowChooseSizeTriggerQV(Boolean showChooseSizeTriggerQV) {
        this.showChooseSizeTriggerQV = showChooseSizeTriggerQV;
    }

    public QuickAddToBasket withShowChooseSizeTriggerQV(Boolean showChooseSizeTriggerQV) {
        this.showChooseSizeTriggerQV = showChooseSizeTriggerQV;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public QuickAddToBasket withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(QuickAddToBasket.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("showPermanentOos");
        sb.append('=');
        sb.append(((this.showPermanentOos == null)?"<null>":this.showPermanentOos));
        sb.append(',');
        sb.append("showMoreInfoRedirectPDP");
        sb.append('=');
        sb.append(((this.showMoreInfoRedirectPDP == null)?"<null>":this.showMoreInfoRedirectPDP));
        sb.append(',');
        sb.append("simpleAddToBasket");
        sb.append('=');
        sb.append(((this.simpleAddToBasket == null)?"<null>":this.simpleAddToBasket));
        sb.append(',');
        sb.append("simpleMobileEmailMe");
        sb.append('=');
        sb.append(((this.simpleMobileEmailMe == null)?"<null>":this.simpleMobileEmailMe));
        sb.append(',');
        sb.append("showEmailMeTriggerQV");
        sb.append('=');
        sb.append(((this.showEmailMeTriggerQV == null)?"<null>":this.showEmailMeTriggerQV));
        sb.append(',');
        sb.append("showChooseSizeTriggerQV");
        sb.append('=');
        sb.append(((this.showChooseSizeTriggerQV == null)?"<null>":this.showChooseSizeTriggerQV));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.simpleAddToBasket == null)? 0 :this.simpleAddToBasket.hashCode()));
        result = ((result* 31)+((this.showEmailMeTriggerQV == null)? 0 :this.showEmailMeTriggerQV.hashCode()));
        result = ((result* 31)+((this.showChooseSizeTriggerQV == null)? 0 :this.showChooseSizeTriggerQV.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.showPermanentOos == null)? 0 :this.showPermanentOos.hashCode()));
        result = ((result* 31)+((this.simpleMobileEmailMe == null)? 0 :this.simpleMobileEmailMe.hashCode()));
        result = ((result* 31)+((this.showMoreInfoRedirectPDP == null)? 0 :this.showMoreInfoRedirectPDP.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof QuickAddToBasket) == false) {
            return false;
        }
        QuickAddToBasket rhs = ((QuickAddToBasket) other);
        return ((((((((this.simpleAddToBasket == rhs.simpleAddToBasket)||((this.simpleAddToBasket!= null)&&this.simpleAddToBasket.equals(rhs.simpleAddToBasket)))&&((this.showEmailMeTriggerQV == rhs.showEmailMeTriggerQV)||((this.showEmailMeTriggerQV!= null)&&this.showEmailMeTriggerQV.equals(rhs.showEmailMeTriggerQV))))&&((this.showChooseSizeTriggerQV == rhs.showChooseSizeTriggerQV)||((this.showChooseSizeTriggerQV!= null)&&this.showChooseSizeTriggerQV.equals(rhs.showChooseSizeTriggerQV))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.showPermanentOos == rhs.showPermanentOos)||((this.showPermanentOos!= null)&&this.showPermanentOos.equals(rhs.showPermanentOos))))&&((this.simpleMobileEmailMe == rhs.simpleMobileEmailMe)||((this.simpleMobileEmailMe!= null)&&this.simpleMobileEmailMe.equals(rhs.simpleMobileEmailMe))))&&((this.showMoreInfoRedirectPDP == rhs.showMoreInfoRedirectPDP)||((this.showMoreInfoRedirectPDP!= null)&&this.showMoreInfoRedirectPDP.equals(rhs.showMoreInfoRedirectPDP))));
    }

}
