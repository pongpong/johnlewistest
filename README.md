### READ ME

#### Get started

1. Build the project using following command 

```shell
mvn clean package
```

2. Under product-web/target directory, Start the server using 

```shell
java -jar product-web-0.0.1-SNAPSHOT.jar
```

3. Open the browser and access the following url

```
http://localhost:8080/products/discount
http://localhost:8080/products/discount?labelType=ShowPercDscount
```